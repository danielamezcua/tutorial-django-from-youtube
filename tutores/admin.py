# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import Tutor, Curso

admin.site.register(Tutor)
admin.site.register(Curso)
from django.contrib import admin

# Register your models here.
