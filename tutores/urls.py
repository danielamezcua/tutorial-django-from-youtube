from django.conf.urls import url
from . import views


app_name = 'tutores'
urlpatterns = [
    #/tutor/
    url(r'^$',views.IndexView.as_view(), name='index'), #name

    #/tutor/71
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detalle'),
]