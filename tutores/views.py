# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views import generic
from .models import Tutor

class IndexView(generic.ListView):
    template_name='tutores/index.html'

    def get_queryset(self):
        return Tutor.objects.all()

class DetailView(generic.DetailView):
    model = Tutor
    template_name="tutores/detalle.html"