# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Tutor(models.Model):
    nombre = models.CharField(max_length=250, default="")
    fechaDeNacimiento = models.DateField()
    direccion = models.CharField(max_length=500)
    ciudad = models.CharField(max_length=30)

    def __str__(self):
        return self.nombre + " | " + self.ciudad

class Curso(models.Model):
    tutor = models.ForeignKey(Tutor, on_delete=models.CASCADE,default=1)
    nombre = models.CharField(max_length=250)
    descripcion = models.CharField(max_length=1000)
    fechaDeInicio = models.DateField()
    terminado = models.BooleanField(default=False)


    def __str__(self):
        return self.nombre
